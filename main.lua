-- global
debug_watch = {}
sounds = {} -- yep, global :D

-- classes
local Gamestate = require("Gamestate")
local Scene = require("Scene")
local Entity = require("Entity")
local Inputstate = require("Inputstate")

-- data
local components = require("components")
local aspects = require("aspects")
local systems = require("systems")

-- internal
local accum = 0
local step = 1 / 60 -- time period between logical updates
local simulation = false
local ending_sound_played = false
local context = {}

-- window
local window_width = 960
local window_height = 540
local window_title = "Arbitrary snake"
local window_flags = { vsync = true }
local font_large, font_medium, font_tiny

function love.load()
  -- love2d stuff
  love.window.setMode(window_width, window_height, window_flags)
  love.window.setTitle(window_title)
  font_tiny = love.graphics.newFont("iceland-regular.ttf", 24)
  font_medium = love.graphics.newFont("iceland-regular.ttf", 36)
  font_large = love.graphics.newFont("iceland-regular.ttf", 64)
  love.graphics.setFont(font_tiny)
  sounds["player_food"] = love.audio.newSource("food_player.wav", "static")
  sounds["player_bite"] = love.audio.newSource("bite_player.wav", "static")
  sounds["enemy_food"] = love.audio.newSource("food_enemy.wav", "static")
  sounds["enemy_bite"] = love.audio.newSource("bite_enemy.wav", "static")
  sounds["victory"] = love.audio.newSource("victory.wav", "static")
  sounds["lose"] = love.audio.newSource("lose.wav", "static")

  -- game stuff
  context.scene = Scene.new()
  context.inputstate = Inputstate.new()
  context.gamestate = Gamestate.new()

  function create_snake(name, extratag, position, color, speed)
    local tail = {}

    local head = Entity.new(name, { aspects.unit, components.model })
    head.position = { unpack(position) }
    head.maxspeed = speed
    head.radius = 10
    head.color = { unpack(color) }
    head.inertia = 0.01
    head.tags[extratag .. "_head"] = true

    -- tail
    for i = 1, 20 do
      local piece = Entity.new(name .. "-tail-" .. tostring(i), { aspects.unit })
      piece.position = { unpack(position) }
      piece.maxspeed = speed * math.pow(0.99, i)
      piece.radius = 10 - i / 4
      piece.inertia = 0.00000001
      piece.color = { unpack(color) }
      piece.color[4] = piece.color[4] / 2
      piece.tags[extratag .. "_tail"] = true

      if i == 1 then
        piece.target = head
      else
        piece.target = tail[i-1]
      end

      table.insert(tail, piece)
    end

    context.scene:addMany(tail)
    context.scene:add(head)
    return head, tail
  end

  local player_head, player_tail = create_snake("player", "game_player", {50, 0}, {64, 255, 64, 255}, 256)
  local enemy_head, enemy_tail = create_snake("enemy", "game_enemy", {-50, 0}, {255, 64, 64, 255}, 256)

  player_head.model = context.gamestate.player
  enemy_head.model = context.gamestate.enemy
  player_tail[1].tags.game_inactive = false
  enemy_tail[1].tags.game_inactive = false

  for i = 1, 3 do
    local food_piece = Entity.new("food-" .. tostring(i), { aspects.dynamic, aspects.drawable, aspects.logical })
    local x = love.math.random() * 920 - 460 -- TODO: this is bad, depends on window size
    local y = love.math.random() * 500 - 250
    food_piece.position = {x, y}
    food_piece.color = {255, 255, 64, 255}
    food_piece.radius = 5
    food_piece.tags.game_food = true
    context.scene:add(food_piece)
  end
end

function love.update(dt)
  if context.gamestate.winner then
    simulation = false
    -- TODO: stop game, congratulate winner, ask to start again
  end

  if simulation then
    -- fix derpiness of love with absense of mousemoved event
    local mx, my = love.mouse.getPosition()
    context.inputstate:mousemoved(mx, my)

    accum = accum + dt

    local updates = math.floor(accum / step)
    -- print(updates)
    if updates > 5 then
      -- skip updates
    else
      systems.input.process(context.scene, context.inputstate)
      for i = 1, updates do
        systems.physics.update(context.scene, step, accum)
        systems.logics.update(context.scene, step, accum, context.gamestate)
      end
      context.inputstate:resetEvents()
    end

    accum = accum - updates * step
  end
end

function love.draw()
  if simulation then
    love.graphics.setFont(font_large)
    love.graphics.setColor({64, 255, 64, 255})
    love.graphics.printf(tostring(context.gamestate.player.length),
                         960 / 2 - 50, 540 / 4 - 64, 40, "right")
    love.graphics.setColor({255, 64, 64, 255})
    love.graphics.printf(tostring(context.gamestate.enemy.length),
                         960 / 2 + 10, 540 / 4 - 64, 40, "left")

    love.graphics.setFont(font_tiny)
    love.graphics.translate(window_width / 2, window_height / 2)

    systems.graphics.draw(context.scene, step, accum)

    -- debug
    local debug_watch_list = {}
    for name, value in pairs(debug_watch) do
      table.insert(debug_watch_list, name .. ": " .. tostring(value))
    end

    debug_watch_string = table.concat(debug_watch_list, "\n")
    love.graphics.print(debug_watch_string)
  else
    love.graphics.setFont(font_large)
    if context.gamestate.winner then
      if context.gamestate.winner == "player" then
        love.graphics.setColor({64, 255, 64, 255})
        love.graphics.printf("Yay, you won! Try again!", 0, 16, 960, "center")
        if not ending_sound_played then
          sounds["victory"]:play()
          ending_sound_played = true
        end
      elseif context.gamestate.winner == "enemy" then
        love.graphics.setColor({255, 64, 64, 255})
        love.graphics.printf("Oh, poor you... Wanna try again?", 0, 16, 960, "center")
        if not ending_sound_played then
          sounds["lose"]:play()
          ending_sound_played = true
        end
      else
        error("Oops!")
      end
    end
    love.graphics.setColor({255, 255, 255, 255})
    love.graphics.printf("To start press <space>", 0, 80, 960, "center")

    love.graphics.setFont(font_medium)
    love.graphics.printf("Rules:", 0, 180, 960, "center")
    love.graphics.printf("1. You are snake", 0, 220, 960, "center")
    love.graphics.printf("2. Your enemy is a snake too", 0, 260, 960, "center")
    love.graphics.printf("3. Chewing slows you down for a second", 0, 300, 960, "center")
    love.graphics.printf("4. If you're hungry, try to bite someone's tail!", 0, 340, 960, "center")

    love.graphics.setColor({255, 255, 255, 32})
    love.graphics.setFont(font_tiny)
    love.graphics.printf("game is made by denis.karabaza@gmail.com", 0, 480, 960, "center")
    love.graphics.printf("font Iceland is made by Victor Kharyk", 0, 500, 960, "center")
  end

end

function love.keypressed(key, isrepeat)
  if key == 'q' then
    love.event.quit()
  end
  if key == " " then -- love2d...
    if not simulation then
      simulation = true
      ending_sound_played = false
      context.gamestate:reset()

      local player_head = context.scene:getFirstTagged("game_player_head")
      local enemy_head = context.scene:getFirstTagged("game_enemy_head")
      local player_tail = context.scene:getTagged("game_player_tail")
      local enemy_tail = context.scene:getTagged("game_enemy_tail")

      player_head.position = {50, 0}
      enemy_head.position = {-50, 0}
      for name, player_tail_piece in pairs(player_tail) do
        player_tail_piece.position = {50, 0}
      end
      for name, enemy_tail_piece in pairs(enemy_tail) do
        enemy_tail_piece.position = {-50, 0}
      end
    end
  end
  context.inputstate:keypressed(key)
end

function love.keyreleased(key)
  context.inputstate:keyreleased(key)
end

function love.mousepressed(x, y, button)
  context.inputstate:mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
  context.inputstate:mousereleased(x, y, button)
end