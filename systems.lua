-- helper function
function has_any (taglist)
  return function (entity)
    for _, tagname in ipairs(taglist) do
      if entity.tags[tagname] then return true end
    end
    return false
  end
end



local systems = {}

systems.physics = {
  update = function (scene, step, accum)
    local entities = scene:getTagged("dynamic")
    for name, entity in pairs(entities) do
      systems.physics.predict_(entity, step, step, entity)
    end
    -- TODO: detect collisions
  end,

  -- yes, it's very ineffective
  detect_collisions_ = function (scene, target)
    local collisions = {}

    local entities = scene:getTagged("dynamic")
    -- local tx, ty = unpack(target.position)
    for name, entity in pairs(entities) do
      if systems.physics.is_collided_(target, entity) then
        collisions[name] = entity
      end
    end

    return collisions
  end,

  is_collided_ = function (one, another)
    local dist = systems.physics.calc_distance_(one, another)
    return dist < (one.radius + another.radius)
  end,

  calc_distance_ = function (one, another)
    local ox, oy = unpack(one.position)
    local ax, ay = unpack(another.position)

    local dx = ox - ax
    local dy = oy - ay
    local dist = math.sqrt(dx*dx + dy*dy)
    return dist
  end,

  predict = function (entity, step, accum)
    local predicted = setmetatable({}, { __index = entity })
    systems.physics.predict_(entity, step, accum, predicted)
    return predicted
  end,

  predict_ = function (entity, step, accum, target)

    local x, y = unpack(entity.position)
    local vx, vy = unpack(entity.velocity)

    if entity.tags.movable then
      local dx, dy = unpack(entity.direction)

      local a = math.pow(entity.inertia, accum)
      local speedmod = math.pow(a, 0.25) * entity.speedmod + (1 - math.pow(a, 0.25)) * 1

      vx = a * vx  +  (1 - a) * dx * entity.maxspeed * speedmod
      vy = a * vy  +  (1 - a) * dy * entity.maxspeed * speedmod

      target.speedmod = speedmod
    end

    x = x + vx * accum
    y = y + vy * accum

    target.position = {x, y}
    target.velocity = {vx, vy}
  end
}

systems.logics = {
  update = function (scene, step, accum, gamestate)
    assert(gamestate ~= nil)

    local player_head = scene:getFirstTagged("game_player_head")
    local enemy_head = scene:getFirstTagged("game_enemy_head")
    local player_tail = scene:getTagged("game_player_tail")
    local enemy_tail = scene:getTagged("game_enemy_tail")
    local food = scene:getTagged("game_food")

    local piece_dist = math.huge
    local piece_closest = player_head
    if enemy_head.model:duration("fed") < 0.5 then
      if player_head.model:duration("slow") < 5 then
        if player_head.model.length > 4 then
          piece_dist = systems.physics.calc_distance_(enemy_head, player_head)
          piece_closest = player_head
        end
      end
    end

    -- process food
    for name, food_piece in pairs(food) do
      local food_piece_taken_by = nil

      if systems.physics.is_collided_(food_piece, player_head) then
        gamestate.player:feed()
        food_piece_taken_by = "player"
      end
      if systems.physics.is_collided_(food_piece, enemy_head) then
        gamestate.enemy:feed()
        food_piece_taken_by = "enemy"
      end

      if food_piece_taken_by then
        if food_piece_taken_by == "player" then
          sounds["player_food"]:setVolume(1)
          sounds["player_food"]:stop()
          sounds["player_food"]:play() -- global
        elseif food_piece_taken_by == "enemy" then
          local dist = systems.physics.calc_distance_(player_head, enemy_head)
          local volume = math.pow(1 - dist / 1000, 2) -- something along those lines
          sounds["enemy_food"]:setVolume(volume)
          sounds["enemy_food"]:stop()
          sounds["enemy_food"]:play() -- global
        end

        local x = love.math.random() * 920 - 460 -- TODO: this is bad, depends on window size
        local y = love.math.random() * 500 - 250
        food_piece.position = {x, y}
      end

      -- calc closest piece to enemy
      local new_dist = systems.physics.calc_distance_(enemy_head, food_piece)
      if new_dist < piece_dist then
        piece_dist = new_dist
        piece_closest = food_piece
      end

      enemy_head.target = piece_closest
    end

    -- process bites
    -- TODO: implement self-biting, maybe?
    for name, enemy_tail_piece in pairs(enemy_tail) do
      if systems.physics.is_collided_(player_head, enemy_tail_piece) then
        if not enemy_tail_piece.tags.game_inactive then
          gamestate.player:bite(gamestate.enemy)
        end
      end
    end
    for name, player_tail_piece in pairs(player_tail) do
      if systems.physics.is_collided_(enemy_head, player_tail_piece) then
        if not player_tail_piece.tags.game_inactive then
          gamestate.enemy:bite(gamestate.player)
        end
      end
    end

    -- general stuff
    local entities = scene:getTagged("smart")
    for name, entity in pairs(entities) do
      systems.logics.update_direction_(entity)
    end

    gamestate:update(step)

    if gamestate.player:duration("slow") > 0.5 then
      player_head.speedmod = 0.5
    end
    if gamestate.enemy:duration("slow") > 0.5 then
      enemy_head.speedmod = 0.5
    end

    local player_length = gamestate.player.length
    for i = 1, 20 do
      local piece = scene:getNamed("player-tail-" .. tostring(i))
      piece.tags.game_inactive = i > player_length
    end

    local enemy_length = gamestate.enemy.length
    for i = 1, 20 do
      local piece = scene:getNamed("enemy-tail-" .. tostring(i))
      piece.tags.game_inactive = i > enemy_length
    end
  end,

  update_direction_ = function (entity)
    if entity.target ~= nil then
      local x, y = unpack(entity.position)
      local tx, ty = unpack(entity.target.position)

      local dx = tx - x
      local dy = ty - y

      local factor = math.sqrt(dx*dx + dy*dy)
      if factor == 0 then
        entity.direction = {0, 0}
      else
        entity.direction = {dx / factor, dy / factor}
      end
    end
  end,
}

systems.camera = {
  predict = function (scene, step, accum)
    local player = scene:getNamed("player")
    return systems.physics.predict(player, step, accum)
  end
}

systems.graphics = {
  draw = function (scene, step, accum)
    local camera = systems.camera.predict(scene, step, accum)
    local cx, cy = unpack(camera.position)
    love.graphics.translate(-cx, -cy)

    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.rectangle("line", -480, -270, 960, 540)

    local entities = scene:getTagged("drawable")
    for name, entity in pairs(entities) do
      if not entity.tags.game_inactive then
        local predicted = systems.physics.predict(entity, step, accum)

        local x, y = unpack(predicted.position)
        local vx, vy = unpack(predicted.velocity)

        love.graphics.setColor(unpack(entity.color))
        love.graphics.circle("fill", x, y, entity.radius, 5) -- TODO: draw something reasonable

        if entity.tags.model then
          love.graphics.setColor({0, 0, 0, 255})
          love.graphics.circle("fill", x, y, entity.radius / 2 + 1, 5)

          local slow_duration = entity.model:duration("slow")
          local fed_duration = entity.model:duration("fed")
          if slow_duration > 0 then
            love.graphics.setColor({64, 64, 255, 255})
            love.graphics.circle("line", x, y, entity.radius + 3, 5)
            local slow_duration_str = string.format("%.1f", slow_duration)
            love.graphics.printf(slow_duration_str, x - 50, y - 12, 30, "right")
          end
          if fed_duration > 0 then
            local fed_duration_str = string.format("%.1f", fed_duration)
            love.graphics.setColor({255, 255, 64, 255})
            love.graphics.printf(fed_duration_str, x + 30, y - 12, 30, "left")
            love.graphics.circle("fill", x, y, entity.radius / 2, 5)
          end
        end
      end
    end
  end
}

systems.input = {
  process = function (scene, inputstate)
    local player = scene:getNamed("player")
    local dx, dy = systems.input.eightway_(inputstate)
    player.direction = {dx, dy}
  end,

  eightway_ = function (inputstate)
    -- TODO: figure out if love2d uses scancodes or not
    --       because not everyone has qwerty layout
    local up = inputstate.keys["w"] or inputstate.keys["up"]
    local down = inputstate.keys["s"] or inputstate.keys["down"]
    local left = inputstate.keys["a"] or inputstate.keys["left"]
    local right = inputstate.keys["d"] or inputstate.keys["right"]

    local x, y = 0, 0
    if up and not down then y = -1 end
    if down and not up then y = 1 end
    if left and not right then x = -1 end
    if right and not left then x = 1 end

    if x ~= 0 and y ~= 0 then
      local factor = math.sqrt(2) -- 1.414...
      x = x / factor
      y = y / factor
    end

    return x, y
  end
}

return systems