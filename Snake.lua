local Snake = {}
local SnakeMethods = {}

function Snake.new(name)
  local snake = {
    name = name,
    length = 5, -- including head
    effects = {} -- <name :string, duration [s] :number >
  }

  return setmetatable(snake, { __index = SnakeMethods })
end

function SnakeMethods:bite(another)
  if self:duration("fed") <= 0 then -- hungry
    if another.length > 1 then
      if self.name == "player" then
        sounds["player_bite"]:stop()
        sounds["player_bite"]:play() -- global
      elseif self.name == "enemy" then
        sounds["enemy_bite"]:stop()
        sounds["enemy_bite"]:play() -- global
      end

      self:apply("fed", 1)
      
      another:apply("slow", 2)
      another:shrink()
    end
  end
end

function SnakeMethods:feed()
  self:apply("fed", 2)
  self:apply("slow", 1)
  self:grow()
end



-- low level

function SnakeMethods:reset()
  self.length = 5
  self.effects = {}
end

function SnakeMethods:grow(n)
  n = n or 1
  self.length = self.length + n
end

function SnakeMethods:shrink(n)
  n = n or 1
  self.length = self.length - n

  if self.length <= 0 then
    error("something is wrong, " .. self.name .. " length reached 0")
  end
end

function SnakeMethods:duration(name)
  return self.effects[name] or 0
end

function SnakeMethods:apply(name, duration, replace)
  assert(name ~= nil)
  assert(duration ~= nil)
  replace = replace or false

  local current = self.effects[name] or 0
  if replace then
    self.effects[name] = duration
  else
    self.effects[name] = current + duration
  end
end

function SnakeMethods:update(step)
  for name, duration in pairs(self.effects) do
    local new_duration = duration - step
    if new_duration <= 0 then
      self.effects[name] = nil
    else
      self.effects[name] = new_duration
    end
  end
end

return Snake