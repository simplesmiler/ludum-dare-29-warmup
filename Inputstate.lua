local Inputstate = {}
local InputstateMethods = {}

function Inputstate.new()
  local inputstate = {
    keys = {}, -- keyboard keys
    mbuttons = {}, -- mouse buttons
    mposition = {0, 0}, -- last registred mouse position
    events = {} -- list of events since last reset
  }

  return setmetatable(inputstate, { __index = InputstateMethods })
end

function InputstateMethods:resetEvents()
  self.events = {}
end

function InputstateMethods:addEvent(event)
  table.insert(self.events, event)
end

function InputstateMethods:keypressed(key)
  local last = self.keys[key]
  if not last then -- key was released before
    self.keys[key] = true
    self:addEvent({ source = "keypressed", key = key })
  end
end

function InputstateMethods:keyreleased(key)
  local last = self.keys[key]
  if last then -- key was pressed before
    self.keys[key] = nil
    self:addEvent({ source = "keyreleased", key = key })
  end
end

function InputstateMethods:mousemoved(x, y)
  local lx, ly = unpack(self.mposition)
  if x ~= lx or y ~= ly then -- mouse was in different spot
    self.mposition = {x, y}
    self:addEvent({ source = "mousemoved", x = x, y = y })
  end
end

function InputstateMethods:mousepressed(x, y, button)
  local last = self.mbuttons[button]
  if not last then -- button was released
    self:mousemoved(x, y)
    self.mbuttons[button] = true
    self:addEvent({ source = "mousepressed", button = button, x = x, y = y })
  end
end

function InputstateMethods:mousereleased(x, y, button)
  local last = self.mbuttons[button]
  if last then -- button was pressed
    self:mousemoved(x, y)
    self.mbuttons[button] = nil
    self:addEvent({ source = "mousereleased", button = button, x = x, y = y })
  end
end

return Inputstate