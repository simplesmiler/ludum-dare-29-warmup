local components = {}

local component = function (name, default)
  return function (entity)
    entity[name] = default
    entity.tags[name] = true
  end
end


-- it's not necessary to verify, if entity already contains component or not
-- the worst thing that may happen isn't that bad :D


-- phisical
components.position  = component("position", {0, 0})
components.velocity  = component("velocity", {0, 0})
components.direction = component("direction", {0, 0})
components.inertia   = component("inertia", 0.01)

components.maxspeed = function (entity)
  entity.maxspeed = 0
  entity.speedmod = 1
  entity.tags.maxspeed = true
end


-- visual
components.color  = component("color", {255, 255, 255, 255})
components.radius = component("radius", 10)


-- ai
components.target = component("target", nil)


-- logics
components.model = component("model", nil)


return components