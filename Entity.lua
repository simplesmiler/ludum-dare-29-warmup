local Entity = {}
local EntityMethods = {}

function Entity.new(name, cons)
  assert(name ~= nil, "You should provide name for entity")
  assert(cons ~= nil, "You should provide list of constructors for entity")

  local entity = {
    name = name,
    tags = {}
  }

  -- apply all constructors
  for _, con in ipairs(cons) do
    con(entity)
  end

  return setmetatable(entity, { __index = EntityMethods })
end

return Entity