local aspects = {}
local components = require("components")

aspects.dynamic = function (entity)
  components.position(entity)
  components.velocity(entity)

  entity.tags.dynamic = true
end

aspects.drawable = function (entity)
  components.position(entity)
  components.color(entity)
  components.radius(entity)

  entity.tags.drawable = true
end

aspects.movable = function (entity)
  aspects.dynamic(entity)
  components.direction(entity)
  components.maxspeed(entity)
  components.inertia(entity)

  entity.tags.movable = true
end

aspects.smart = function (entity)
  aspects.movable(entity)
  components.target(entity)

  entity.tags.smart = true
end

aspects.unit = function (entity)
  aspects.drawable(entity)
  aspects.smart(entity)

  entity.tags.unit = true
end

return aspects