local Scene = {}
local SceneMethods = {}

function Scene.new()
  local scene = {
    all = {}
  }

  return setmetatable(scene, { __index = SceneMethods })
end

function SceneMethods:add(entity)
  local name = entity.name -- TODO: make entity class
  self.all[name] = entity
end

function SceneMethods:addMany(entities)
  for _, entity in ipairs(entities) do
    local name = entity.name
    self.all[name] = entity
  end
end

function SceneMethods:delNamed(name)
  assert(name ~= nil, "You should provide entity name to delete")

  local entity = self.all[name]
  assert(entity ~= nil, "No such entity") -- TODO: replace assert with some kind of warning

  self.all[name] = nil
  -- TODO-FIX: some reference may remain inside
end

function SceneMethods:getNamed(name)
  assert(name ~= nil, "You should provide entity name")

  local entity = self.all[name]
  assert(entity ~= nil, "No such entity") -- TODO: replace assert with some kind of warning

  return entity
end

-- TODO: implement :getTaggedMultiple(list of lists of tags)
function SceneMethods:getTagged(tagname)
  local filtered = {}

  for name, entity in pairs(self.all) do
    if entity.tags[tagname] then
      filtered[name] = entity
    end
  end

  return filtered
end

function SceneMethods:getFirstTagged(tagname)
  for name, entity in pairs(self.all) do
    if entity.tags[tagname] then
      return entity
    end
  end

  return nil
end

function SceneMethods:getTaggedAny(taglist)
  local filtered = {}

  for name, entity in pairs(self.all) do
    for _, tagname in ipairs(taglist) do
      if entity.tags[tagname] then
        filtered[name] = entity
        break
      end
    end
  end

  return filtered
end

function SceneMethods:getFiltered(filter)
  assert(filter ~= nil, "You should provide filter")

  local filtered = {}
  for name, entity in pairs(self.all) do
    if filter(entity) then
      filtered[name] = entity
    end
  end

  return filtered
end

return Scene