local Snake = require("Snake")

local Gamestate = {}
local GamestateMethods = {}

function Gamestate.new()
  local gamestate = {
    player = Snake.new("player"),
    enemy = Snake.new("enemy"),
    winner = nil
  }

  return setmetatable(gamestate, { __index = GamestateMethods })
end

function GamestateMethods:update(step)
  self.player:update(step)
  self.enemy:update(step)

  self:check()
end

function GamestateMethods:check()
  if self.enemy.length >= 20 then
    self.winner = "enemy"
  end
  if self.player.length >= 20 then
    self.winner = "player"
  end
end

function GamestateMethods:reset()
  self.player:reset()
  self.enemy:reset()
  self.winner = nil
end

return Gamestate